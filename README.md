# Child Artifacts

Relates to: https://gitlab.com/groups/gitlab-org/-/epics/8205

## Getting started

Until GitLab provides a native way to pull in artifacts into the parent pipeline, this
is a workaround.

Three Key Constraints

1. There is a constraint for `needs:project` in that it only supports 5 `needs` by default. This [can be overriden](https://gitlab.com/gitlab-org/gitlab/-/issues/207329#note_1579807578). 
1. Every child job's name must be unique as you use `needs:project` in parent to reference child.
1. All artifact file names must be unique - leverage CI Job Name or ID as a suffix.

Other info

1. This works for Merge Request Pipelines and Branch Pipelines - with each one using a different job.
1. This is written verbosely. Use `extends:` to reuse as much as possible.
